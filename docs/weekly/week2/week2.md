#02. BIOLOGY ZERO

#Jonathan Minchin - Architect

#Nuria Conde - Synthetic and Computational Biologist

#Monday 8 October 2018


MORNING

- We are told we can all become biologists and that genetic engineering provides us with tools for the future. Today biology can be divided into synthetic biology, computational biology and genetic biology. The next two weeks, apart from learning new things related to biology, are to encourage us to explore the notion that biology is something that the average person can partake in and even do at home.

- Nuria, a synthetic and computational biologist, tells us that synthetic biology is about trying to make organisms do what we want, she calls them ‘mutants’ and points out how we can now ‘edit’ life.

- I’m not so sure this notion fits with my desire to be part of the Nature. It seems more like playing god in a world where we have already shown we are totally incompetent at even coexisting, let alone messing with the complex flux of life. The idea that we experiment with life by playing with a microscopic element that has evolved over millions of years before we even have the slightest notion of how it really works or even came in to being appears to be part of human arrogance and potentially very dangerous.

- I’m all for exploration and the design process is all about taking things apart and reassembling them in new ways to create new things but somehow Nature is something infinitely more complex than we can imagine. So for me the human desire to make a foray into manipulating Nature before we even understand it seems to follow the same suicidal path to destruction the human race is currently racing towards. We have developed a society that is based on a massive amount of misguided creativity all based on a single notion that we can successfully create a society based on extracted fossil fuels. Somehow playing with DNA to create mutants doesn’t sound like it is going to end well.

- Nuria explains that everything started with the big bang, (which is a highly contested notion.) I prefer to go along with Bertrand Russell that there is no reason to believe the universe has beginning at all, which is due to the poverty of our imagination. From there Nuria explains the that the order of things begins with atoms, which form clusters of molecules and then molecular biology and eventually a cell. This is a massive leap in only four steps that has taken billions of years and perhaps even more. For me this simplification shows that we really don’t know, in this trillionth of a second that humans have existed in the scheme of things, and we somehow feel we know enough to tamper with this process.

- But perhaps this is our destiny, that we are creatives and part of the creative flux and we are simply doing our creative thing just as everything else the universe is doing. Perhaps even though we think we are doing our experiments consciously we are in fact simply driven by the creativity that is us - and like other experimental creatures this will work out to our advantage or to our self annihilation.

- Nuria tells us that in Genetic Engineering the ‘ultimate weapon’ is CRISP-Cas9, this military term seems to imply that it is already being used to create questionable results. She shows us photos of miniature pigs and explains how in China they are modifying pigs to be more muscular so they produce more meat. What are the ethics in all this? We are also using pigs for human transplant parts because their makeup is so similar to humans - do pigs have any rights? Does life have any rights? Apparently if you want to modify yourself it is also legal and the biologists are also close to being able to create life! What sort of life I wonder?

- Nuria tells us confidently that it is possible for humans to have wings but that we would not be able to fly. This is interesting that we can modify the form and specific characteristics of life with Genetic engineering but cannot yet modify the sensory pathways and consciousness needed to make a human fly. We can’t talk to the miniature or muscular pigs that have been modified but I wonder how they feel, are they different somehow in their consciousness in their being that we cannot know?

- Nuria asks us if consciousness is natural then why is a synthetic organism unnatural. This seem like a strange question!, I’m not sure I understand the question. Consciousness is something we have and are born with. A synthetic organism is something that has not evolved, rather it is something that has evolved and has been modified by humans with really little understanding about the consequences, however we might observe differences. Of course it’s all Nature but the synthetic organism has been modified to fit a human construct. I realise now that the chapter in my PhD on the distinction between Nature with a capital ’N’ meaning Nature that has not been affected by humans and nature with a small ’n’ referring to Nature that has had a human construct applied to it needs revising. I didn’t include or foresee synthetic organisms back then. Everything is Nature and within that Nature there is another nature of human constructs that modify Nature. I think there is a distinction been the two.

- I liked Jonathan’s challenge: To create a new animal that does not have any component of an existing animal. It is of course beyond our abilities to conceive something that has no reference to our known universe. As designers and Architects it seems we are destined to be limited to what we can learn or experience and translate it into material and non material manifestations but we cannot invent something without a prior reference to our know universe.

- Nuria teaches us that a 3D printer can be modified to print human tissue. The Organovo Novo Gen MMX Bioprinter using thermo hydrogel materials mixed with blood cells can print out tubular veins that mimic human blood vessels. Who’s blood will be used? From here we start to ask where raw materials come from and are they recoverable. Most current processes do not allow or consider the recovery of materials. Here the notion of a circular economy surfaces, a concept that Ellen MacArthur has engaged with together with some of the world’s largest corporations. I have a problem with her methodology in that the corporations exist not for public good but for profit and perpetual growth. This means that the corporations use this process to continue grow and to continue to extract resources and exploit labour.

- I would rather see a circular economy working at a localised Fab Lab level where there are not materials flowing in or out of a city, rather everything is part of a circular economy within the boundaries of the city such that the only additive (as in Krebs cycle) is a single form of energy and in the case of the city this could be sunlight. Maybe the Circular Economy should be called the Circular City to give it a specific focus or more specifically ‘Circular Barcelona’?

- We continue to discuss materials and I sense that a lot of people are developing materials independent of a purpose and so we end up with materials in search of applications which is poorly spent time to go in search of possible applications. Perhaps reversing this and looking at authentic human needs and then finding the most ecological solution to the problem is a better way to proceed. In this manner any new material that emerges as the best ecological solution begins to inform similar applications.

AFTERNOON

- In the afternoon we make a culture that we will inoculate with a bacteria we will find close to IAAC.

- We begin by mixing the ingredients to make a Lactobacillus culture in our group’s case.

![](Lactobacillus.jpg)
Preparation of Lactobacillius culture. Photo: Jonathan Crinion

- We are coached on sterility methods and asked to think of where we might find a bacteria sample to inoculate our culture with. I chose to take a sample from the exterior door handle of the IAAC  Atelier. My assumptions is that I’ll get a good sample of the bacteria from all the people that arrive and open the main entrance door. As you will see in the culture tomorrow there is some good colonisation action and then the day after it gets even better.

- Making our culture was fun and it was good to remember that every day foods all have specific chemical properties. Our Lactobacillus culture included a squeezed lemon into milk to peptonize it, tomato juice squeezed through a filter and 2.5 gm of agar and a bit of water.

- We poured the culture into petri containers in a sterile environment close to a bunsen burner, which in our case was actually a camping stove burner with a flexible cable. I want to make my next kitchen with a few of these other counter top.

![](Streaking.jpg)
Sterile environment ready for streaking our culture with bacteria. Photo: Jonathan Crinion

After a short wait the cultures were set and we could inoculate them with our finger after first sterilising it with ethanol and then in my case touching the main door handle and rubbing my finger on the culture.




#BIOLOGY ZERO

#Tuesday 9 October 2018


MORNING

- Today was about Biohacking and then later about Biochemistry. But first we checked our cultures from yesterday and I was excited to see that mine was beginning to show some small areas of contamination.

![](Door1.jpg)
My culture showing bacteria spreading. Photo: Jonathan Crinion

- There was a big emphasis to start on biohacking - making and finding equipment to replace the standard very expensive professional lab equipment that was excessively priced and in general easy to make you own. Also using materials and tools you might find at home in your kitchen.

- As a designer I have designed a few pieces of medical equipment and I was always shocked at how low cost it was to make and how an item costing only 3 to 5 dollars to manufacture would sell for thousands. In one instance I recall researching medical refrigerators only to discover they were standard off the shelve refrigerators bought from Bosch that had medical name on them and the shelves were replaced with stainless steel ones. Somehow that change added more than a thousand dollars to the price!

- So the idea of building our own equipment sounded like fun and Nuria showed us a few things she had made, what an amazing woman! I especially liked her dremel tool with a 3D printed head to spin test tubes and thus make a hand held centrifuge.

- It is important to say that throughout all this Jonathan was on hand and did a fantastic job of translating Nuria’s sometimes extremely complex ideas and knowledge into a simple understandable story. Without Jonathan I’m not so sure we would have survived as the complexity increased - they make a good team.

- Nuria explained the names of various bits of equipment from beakers, flasks falcons, pipets, autoclaves, incubators, spectrophotometer, bio reactor, a PCR or Thermal Cycle Machine, electrophoresis and a transilluminator. Interesting equipment and all could be easily made at home. She also gave us the web sites a list of good bio hack web sites, which I’m keen to explore when I have moment.

- Then things start getting more complex after the break as we move into Biochemistry. This talk appeared to me to be about teaching us some of the basics of how life works. Nuria began with describing the functional groups of atoms in organic molecules that are responsible for the characteristic chemical reactions of those molecules. Using an example of how lipids form membranes I got a sense of how tings need to be contained and how having a boundary is what defines the organism. Jonathan pointed out how Nuria’s list of lipids, proteins, nucleic acids and carbohydrates corresponded to form, function, information and energy.

![](Groups.jpg)
Nuria explains the functional groups. : Photo Jonathan Crinion

- The part that interested me the most in all this were the proteins that apparently do not have any DNA and when Nuria said that proteins were organised by electrophysical properties. I wondered if this included the quantum field as my MSc Professors Brian Goodwin and Lynn Margulis convinced us that it was not DNA that was the driver but rather the field was modifying organisms. Given that proteins are a type of material that can form to make a multitude of different objects without any DNA seems to suggest that there is something interesting going on in relation to the electrophysical properties and I’m curious to know more. I’ll eat more amino acids from here on to encourage a good protein balance :-)

- The technique for identifying specific proteins is an interesting methodology. By creating a protein crystal, which makes a ‘geometrically regular form with symmetrically plane faces’ (Mac dictionary 2018) It is possible to bombard the protein with neutrons, which will be reflected from the various facets of the crystal. In this manner it is possible read the reflected pattern of neutrons and thus identify the type of protein.

![](Protein.jpg)
Image presented by Nuria Conde in her presentation. Source unknown.

- We then moved on to DNA, which is an impressive concept given that a computer works on 0 and 1 and DNA has instead the 4 variables Cytosine (C), Guamine (G), Ademine (A) and Thymine (T). I’m surprised that computers have not progressed with this knowledge although I seem to recall it has been done but not commercially as far as I know.  

- Metabolism is the total of all chemical reactions within a cell and certainly this notion of everything being interlinked and related has been carried forward into design. If only we could really incorporate design as a metabolic agent within Nature the world might be a very different place today. The following is quote I like from from New Geographies 06: Grounding Metabolism, which shows the potential for applying the concept of metabolism to urbanism :

‘Confined to the regional scale historically, today’s generalised urbanization is characterized by an unprecedented complexity and planetary upscaling of metabolic relations. Most contemporary discussions of metabolism have failed to integrate formal, spatial, and material attributes. Technoscientific approaches have been limited to a performative interpretation of flows, while more theoretical attempts to interrogate the sociopolitical embeddedness of metabolic processes have largely ignored their formal spatial registration. Within this context, the design disciplines fascinated by the fluidity of metabolic processes have privileged notions of elasticity without regard for the often sclerotic quality of landscapes and infrastructures. The issue addresses the challenges associated with the planetary dimension of contemporary metabolic processes, offers a critical examination of the long lineage of historical discussions and schemes on urban metabolism from the design disciplines and places them in parallel with a set of contemporary projects and interventions that open up new approaches for design.’

Ibanez, D., Katsikis, N., Eds (2014) New Geographies 06: Grounding Metabolism, Harvard University Press, USA

- Using a Spectrometer made by Nuria from an Arduino board with a block of black foam mounted on top with a square hole to fit square containers. A hole has been made on either side of the black foam and on one side is a red LED and on the other is a sensor such that light can be shone through liquid placed in the containers. The square tubes that fit in the foam have saw tooth sides vertically so that the light is concentrated only between the LED and the sensor.

![](biohack.jpg)
Spectrometer made by Nuria. Photo: Jonathan Crinion

- By placing various dilutions of (in our case) coloured water it is possible to measure the amount of light that is passing through the liquid.

- So why would we want to do this? Light is divided into various spectrums of different wave lengths and colour is the propensity to reflect now part of the light spectrum more than the other. From previous experiments scientists know what colour of light will be absorbed by a specific microbe so by choosing a specific light wave length and directing it through a medium with microbes in it we are able to identify the density of the microbes. So this is a very simple method of a process that can be made at home to develop experiments that are part of a larger project.

![](Blue.jpg)
Making up the tubes with different densities. Photo: Jonathan Crinion

- In another similar experiment Nuria showed us how to find the density of calcium carbonate in an egg shell by dissolving a crushed egg shell in a known amount of acid with a 1.0 molar. Once the calcium carbonate has reacted and dissolved an acid neutraliser with trace colouring that reacts when a certain density is achieved and by counting the number of drops and subtracting from the initial quantity of the solution we can arrive at the volume of calcium carbonate. This is a similar experiment to putting an irregular object in a bucket of water and measuring the volume of water that is displaced as a means to find the volume of the irregular object.

- So what all these experiments show us are simple logical ways to locate and identify specific organisms or materials and their specific quantities in relation to other substances or organisms. With these examples I can extrapolate and create my own solutions to other similar types of problems.




#BIOLOGY ZERO

#Wednesday 10 October 2018


MORNING

- First things first - I check my bacteria culture, which has by now grown.

![](Door2.jpg)
Image of my door handle bacteria culture on second day. Photo: Jonathan Crinion

- We begin the day discussing the metabolism of cell and how anabolism, which is about building and bond making that requires an energy input and then catabolism, which is about breaking the bonds of macromolecules and releasing energy. I wonder if this is what happens in metamorphosis as the caterpillars molecules are disassembled the energy must be stored somewhere to then rebuild the molecules into the butterfly?

- Krebs Cycle is a good example of a scenario in which a cyclical exchange produces energy at various points and in the end what is left over is returned to react with a new input to start the whole cycle again. We take in energy through the food we eat to power this cycle and thus keep the cells that are us alive, what a truly remarkable process! We imagine we are feeding ourselves but in fact we are feeding millions of cells which as a whole we experience as a self! Plants are slightly different in that take food from the Earth and by photosynthesis take energy from the Sun.

![](Kreb.jpg)
Nuria explains Krebs Cycle. Photo: Jonathan Crinion

- Next we move on to classification systems either by nutrition or by information. I guess this is a human preoccupation as a means to compare like with like, to give order to the chaos so we can see if there is something that does not fit the categories and might be different somehow.

![](Class.jpg)
Image by Nuria Conde in her presentation

- We move on to microscopes. My father bought me a microscope as a child and I think I looked at just about everything I could lay my hands on under that microscope, it was a tool I used often to explore the things I could not see very well. I remember pee was multi-coloured and I passed the microscope on to one of my daughters that took an interest in it. I also did a lot of work in Biochemistry in high school that required the use of microscopes so I have a lot experience with them and managed to take a break at this point and catch up with my notes.

- After lunch we watch a video that Thora an ex IAAC student has produced of her work. I was impressed with her presentations and that she has managed to find her place in the world continuing her biological work such as painting with living things, working with slime mould, designing clothes that are bioluminescent, biocalyitic cells. Nuria tells us how luminescent bacteria has a circadian rhythm for its luminescence. I wonder if this is something to do with building up the energy in one period so that it has stored the energy needed to light up at night?

- Next Jonathan and Nuria perform and experiment with spirulina. They have a sample of the spirulina and they will make make a new batch by taking a portion and providing it with the nutrition required to grow. Part of that process requires that the water has some iron in it so they are boiling a file with lemon juice and vinegar to help dissolve the iron. I'm wishing they had taken the orange plastic handle of first but I get the concept - I have a Japanese tea pot at home and getting a daily supply of iron is part of the ceremony. They also mix in Bicarbonate Soda, Potassium Nitrate (they could have simply used pee but they would need to take out the ammonia and maybe I would not like to know about it if they had and we had to eat it!), Potassium Sulphate, Magnesium Sulphate, and dechlorinated water. All this mimics the type of volcanic environment that Spirulina likes.

- So this experiment is not only about an amazing way to get some good proteins into our bodies - Spirulina has 70% of all essential (to humans) amino acids and is the best way to create the protein we need - the experiment is also about how to take a sample of a plant and create a situation in which it can thrive and reproduce. It reminds me of making yogurt only a bit more complicated.

![](Spirl.jpg)
Jar of spirulina waiting to be transferred into the solution.




#BIOLOGY ZERO

#Thursday 11 October 2018


MORNING

- This day is about Cellular Biology. Nuria shows us an image of a cut away cell with all the parts labeled. While I know how to take a car engine apart and how it works this diagram is impenetrable in that it is hard to grasp what these abstract objects are. I can only sense how the parts might interact but we are missing so much information as to how it actually works. However it is all starting to come together now, all the bits we have learned about during the week and there is a lot I may never know.

![](Cell.JPG)
Image: 2004 Pearson Education Inc. Publishing as Benjamin Cummings

- I am at once stunned to learn that all cells start off the same as generic cells and each with same DNA. What a remarkable concept! But there is no know 'director' of the orchestra that had the score in front of them to tell these cells how to modify themselves into legs, a heart, a liver, an eyeball and so on. We have a sense of what a cell is and how it keeps itself alive but we have no idea how these cells know how to change and morph into various parts that go together to create a human or a fish or a bug or a plant ...

- We know that the proteins, which have no DNA of their own, organise the cells in the embryo and give them directions to read specific parts of their DNA. Whatever this interaction is seems to allow the cell to evolve into a specific cell with a specific purpose.

- I like the citoskeletal system that is essentially the framework like a human skeleton that gives all the organelles some form of support. When I look at the renderings I imagine a city being built this way, where the membrane is permeable to let rain and light through and at the same time captures some of the water and light to power the human living cells below. The open structure in the image looks wonderful so I imagine it would require a different human economic ideology that was not for perpetual profit to prevent the density of such structures from becoming oppressive.

![](Skeleton.jpg)
https://scienceaid.net/the_Eukaryotic_Cell_Cytoskeleton

- We move on to the 'Central Dogma of Biology'.

- This is given as a truth so I'm not so sure about it all right from the start before I know anything at all. It's a fuzzy concept but seems to be about going from a DNA strand through a process of 'transcription' such that a copy is made, like a photocopy to create an RNA strand. This RNA is in turn 'translated', which is a production process into a protein. This is a process whereby information is turned into something physical or in other words the cell takes coded information that a rhyzome machine can then translate into a physical object. It took a few tries to grasp how this process works and below is my drawing. Nuria added that the bit on the right showing how the RNA uses three connection to find the correct match that will select the amino acid needed for the function.

![](Dogma.jpg)
Sketch from my note book trying to understand how it works

- In retrospect it is a very simple machine but the real question remains, which is how did it know what it wanted to do, what drives this process or how does it know when to stop? I'm really curious how the cell makes sequential decisions and then enacts them. Is this simply a reactive process or does the cell have foresight. I look out my window and I see a whole city, cars trains and aeroplanes, wow theses little cells that made us area amazing.


AFTERNOON

- We explore the genome structure and specifically look at human genetic imprinting of how we inherit out parents genes. I'm fascinated at the possible combinations that we may possibly inherit from out parents and wonder how that affects us, especially in the case where some people rather than having two chromosomes have four! Surely this must make some sort of difference to the complexity of the person?

- This brings us back to the GM discussion. Nuria says we have always active actors in the modification of food. I agree, we select plants that suit our needs over the ones that do not. What appears to be missing the GM concept is an acknowledgment that all biota do not exist in isolation but are instead part of much larger ecosystem. A birch tree for example lives with very specific fungi, insects, bacteria, birds, animals in a very specific soil type in a very specific climate and will be part of a very specific collection of plants etc... that are all part of a localised ecosystem, which is part of a larger ecosystem. So changing one aspect of any one of these participants can have massive effects on the whole ecosystem. I know from experiments I did under James Lovelock the author of Gaia and a NASA scientist that by changing on small component in an ocean ecosystem the entire web of interactions can collapse. As the world becomes more and more fragile GM foods could actually be acting as catalysts to speed up environmental collapse. I wonder if there are any studies on this phenomena?

- We move on to Polymerase Chain Reactions (PCR), which is about how to replicate a piece of DNA. This is apart of a larger project of Epigenetics, the study of changes in organisms caused by their modification of gene expression rather than the alteration of the genetic code itself. Nuria did the experiment for us and I was amazed at how simple it is to replicate a stand of DNA with only a DNA Primer and some spare Nucleotides along with a small contraption that is essentially a heater with two small fans to cool that can heat then decrease the temperature over a cycle and with each cycle the DNA chain replicates.

![](DNArep.jpg)
DNA repliacating machine. Photo: Jonathan Crinion

![](Phasing.jpg)
Image of Nuria's computer screen showing how the replicating machine temperature had gone up and down over a period of time. Photo: Jonathan Crinion

- Nuria then made a gel on a tray with some pockets in it to apply the DNA mixture along with a ultraviolet dye that will allow the DNA to show up later so we can see it.

![](Insertdna.jpg)
Inserting the replicated DNA into the gel. Photo: Jonathan Crinion

By applying a negative current from one side the of tray the electricity flows through the tray and because the DNA is negatively charged (why should it be negatively charged I wondered?) the two negatives oppose each other and so the DNA migrates over about half an hour to the other side of the gel and because there are coloured tracers it is possible to make sure it does not come out the other side of the gel by stopping the electrical current. Amazing really that someone figured this simple process all out. I later watched a video of someone extracting DNA from a banana in their kitchen, which is truly amazing. Although the implications of how easy this all is terrifying when you consider that it is impossible to really understand what changing DNA strands could actually do if they became wild and entire ecosystems could collapse.

![](DNAmove.mp4)
Video taken by a class mate and shared on WhatsApp of the DNA migrating across the gel.

![](DNAstrands.jpg)
The final result illuminated by a blue light. The DNA has migrated across the gel. Photo: Jonathan Crinion        

Jess an Architect and past IAAC student in Computational Biology gives us a presentation of her past work. She presents a range of projects from Hyper Articulated Mycomorph (SP?) Mycelieum, bioplast materials to a composting system and Bacteria dying. the most interesting part is the work with slime mould showing it's capacity to spread out in search of food and then to retreat and reify the paths to the food in the shortest and most efficient possible path. This is all by a fungus with no brain, but rather an agglomeration of individual cells that some how is directed by an unknown force.

![](Jess.jpg)
Jess showing us the Slime Mould she used in her jewellery. Photo: Jonathan Crinion     






#BIOLOGY LAB WEEK 1

#Friday 12 October 2018


MORNING

Today we visited the Barcelona Biomedical Research Park (PRBB) where Nuria works. It's an impressive building in a great location on Barcelona's waterfront overlooking the sea. We have been asked to bring our passports to gain entrance, which makes us all suspicious of what they might be doing inside after our discussions about genetic modification. Nuria gives us a great tour of the building with many anecdotes about how things work. The interesting part is that the PRBB facility is many different companies and institutions all sharing the same building and many of the expensive pieces of equipment that would otherwise not be available.  

![](PRBB.jpg)
Barcelona Biomedical Research Park (PRBB). Photo: Jonathan Crinion

![](Danger.jpg)
Somewhere we are advised not to go! Photo Jonathan Crinion

One thing really sticks with me from the tour and also from the classes and that is the disregard for sentient life. I found it painful to watch a small fish dying on slide so we could look at it through a microscope. You could see it trying to breath and struggling to escape. At the PRBB we were taken the horrific area in the basement of the building where animal experiments are done. We were told to be quiet so as not to disturb the animals that we could not see. Peering through a window I could see the torture cages where animals were kept in small barren stacked cages. The rooms were void spaces of stacked cages and dim light. In my mind these torture chambers alone invalidate any experiment as the animals must all be in an extreme state of fear and stress. I'm reminded of Rene Decartes vivisection experiments where he claimed that the animals feel nothing and their screams are merely nerve responses. I felt very sad to see that science has failed to move beyond this arrogant position that some life is simply disposable. Thank god ness there is a big movement in product labeling so consumers a can choose products that have not been tested on animals.

![](Fly.jpg)
 Coloured Photo on the wall of a Fly's head. Photo of photo: Jonathan Crinion

 ![](Nuria'sworld.jpg)
 Nuria Conde's world. Work area at PRBB. Photo: Jonathan Crinion


#HYPOTHESIS & METHOD

AN IAAC PROPOSAL TO THE CITY OF BARCELONA FOR THE MODIFICATION OF SUPER BLOCKS


#QUESTION:

Where might it possible to grow food and fab lab materials in the city of Barcelona on a large scale as a means to make the city more self supporting.

#HYPOTHESIS:

There is enough unused space in a Barcelona Super Block to grow all the food required to feed the Super Block residents and also it is possible to grow some of the raw materials that may be used by local FabLabs. Thinking of the Super Block as a biological cell will inform the process.

#ABSTRACT:

Super Blocks are a clusters of city blocks where vehicular traffic has been restricted so as to provide pedestrian corridors. The Super Block in Barcelona has been modified and fitted with trees in large plastic containers, benches, street painting to delineate pedestrian areas and restricted vehicular traffic routes, and various entertainment and play structures. The environment appears to be a temporary intervention and the roads remain intact under the non-permanent interventions. Local residents and businesses have formed an opposition group to the city’s intervention ostensibly because they were not consulted about the changes.

A new intervention such as growing food and fab lab materials could be an opportunity to engage the community and add new life to this Super Block intervention by providing an attractive privately managed for profit enterprise that grows food and utilitarian plants for Fab Labs and sells these products at a weekly market stall. The growing area could provide a green area that attracts local residents and businesses to avail themselves of. Additionally this could be an opportunity for local businesses to find ways to engage with the increase in pedestrian traffic which could help revitalise the Super Block, give it a purpose and make it a destination.

#METHODOLOGY:

AIM

If we imagine a Super Block as a biological cell with a metabolism then growing plants becomes part of a much larger process. We can imagine the Super Block’s boundary as a membrane within which a local metabolic population interacts. The cells population become agents with the ability to form a social cohesion for the good of the cell that they occupy. They act as participants in maintaining the metabolism of their Super Block cell. Maintaining and building the structures and business in the cell requires an anabolic process as does the economy of the cell. This building process of anabolism requires various different forms of energy input. At he same time catabolic processes are taking place which is about breaking the bonds of the macromolecules in the form of   food to maintain the population of the cell. The built forms, all with openings onto the street shape a social structure from which the human agents emerge from their security, protection and a place to retire when the Earth turns from the sun. The population re-engages with the metabolic flux of the Super Block cell. The place where people meet to create social cohesion or the nucleus is presently undefined. Some people wake up and leave the cell to work elsewhere, while some people perhaps live and work in the same area and thus a have deeper connection. What do the people that leave the cell and return in the evening contribute to the cell? Creating a cellular identity or nucleus for the block is local market area, which focuses the population in gatherings to exchange information, while replenishing their nutrient storage. Electromagnetic energy is captured from there sun by moss and other means and converted into drying systems to preserve food and to supply energy for illumination. Rain water is captured to feed the plants, food waste can be converted into biogas or used as compost so it is recycled into the food chain that keeps the cell alive. Over time the local economy adapts to the cell structure and creates a symbiotic relation ship. Fab Labs move closer to the source of their materials. Also over time the cell becomes virtually autonomous with only a small amount of external input required to sustain the life within it and becomes a model for a possible city entirely comprised of autonomous cells. This attracts people from all over the world to visit, experience and observe and learn how they may create their own cells where they live.


DATA COLLECTION REQUIRED TO CLARIFY POSSIBLE INTERVENTION.

Existing plan layout and photos.

Map building volumes for sunlight analysis.

Census material to determine population density.

Data on how many people live and work within the Super Block.

What are the closest Fab Labs and what materials do they need

What is the area required to feed one person on a vegetarian diet.

What are the existing site amenities such as water sources, existing plants and obstacles.

Calculation of road surfaces available minus fire route, paths, and delivery traffic etc.

Find direct comparisons to a cell and define links and path ways.

Meet with the City of Barcelona Super Block team and explain project to get feed back.

DATA ANALYSIS

Map all the variables onto the site and Identify any problems.

PRELIMINARY SOLUTIONS AND DESIGN

Prepare various possible scenarios for space usage.

Possible business scenarios to manage, maintain and sell the plants.

What are the things that require funding and staff? Possible funding scenarios.

COMMUNITY ENGAGEMENT

Meet with community and engage in conversations about their involvement and increase in pedestrians traffic etc.. Circle through this process until all involved are in agreement.

REVISED SOLUTIONS AND DESIGN

Integrate learning and problems

FINAL DESIGN PROPOSAL

Prepare and communicate final proposal to city planners, residents and businesses.

Develop funding proposal and staged implementation process to a time schedule

CONCLUSION

To be completed once the final design is known



#REFLECTIONS FROM THE WEEK

- This has been a wonderful week. Nuria and Jonathan are a good team. Nuria's knowledge and comprehension of biology and many other things is simply awesomely stunning. Jonathan's ability to translate Nuria's sometimes very complex concepts into simple easy to understand ideas was also just as amazing because it meant he has already understood the ideas and processes. I have experienced this distinct difference between a mathematical mind and a visual mind before and so it was a treat to see them work together to give us in the end an extremely brilliant week.

I enjoyed remembering just how amazing life is, that we exist because of millions years of evolution and here we are a bundle of microscopic cells all working together to make me - and on top of that I am somehow conscious. When I appreciate this concept and in comparison at a larger scale to Earth's biosphere I get a sense of how fractal the universe is. More importantly when I see how a cell or a conglomeration of cells work at both the individual and at a group level so efficiently, I feel there is so much we can learn from these creatures that can arrive at a whole which is greater than the parts. We haven't as species been able to do that yet it seems but certainly the effects of our unconscious actions are now visible. In all types of cells we have a very good example of how it could be possible, of something that works and that we might consciously imitate.

I also marvel that there must be another ingredient - that which directs all these cells and that which allows them to all work in concert to create me and keep me alive.

When I look at the illustration below I feel very sad because it is not hard to see that life has flourished for so long, that there have been many extinctions and it looks from the frequency of extinctions that we might be next in line . . . and here we are at the last point on the right side of the diagram and with the power to destroy it all.

![](Life.jpg)
Diagram from Nuria Conde's presentation

I wonder if this diagram is showing us clearly that our destiny is to become extinct as well - or - is this point a new beginning and the form of the evolution that brought life this far is about to grow again, that the biodiversity will increase and all the cells in human bodies all over the planet will somehow come together to form a larger organism that works at least as well as a cell's metabolism. We have an infinite number of examples right in front of us showing us the way, lets hope we have the determination to see the opportunity right in front of us to change our ways.
